import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Landing from '../views/landing.vue'

Vue.use(VueRouter)

  const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'landing',
    component: Landing
  },
  {
    path: '/test',
    name: 'test-elektre',
    component: () => import('../views/test.vue')
  },
  {
    path: '/app',
    name: 'app',
    component: () => import('../views/app/index.vue'),
    children: [
      {
        path: 'logout',
        component: () => import('../views/app/logout.vue')
      },
      {
        path: 'suppliers',
        component: () => import('../views/app/supplier/index.vue'),
        children: [
          {
            path: 'create',
            component: () => import('../views/app/supplier/create.vue')
          },
          {
            path: 'edit/:id',
            props: true,
            component: () => import('../views/app/supplier/edit.vue')
          },
          {
            path: '',
            component: () => import('../views/app/supplier/list.vue')
          }
        ]
      },
      {
        path: 'obats',
        component: () => import('../views/app/obat/list.vue')
      },
      {
        path: 'obats/create',
        component: () => import('../views/app/obat/create.vue')
      },
      {
        path: 'obats/:id/supplies/create',
        props: true,
        component: () => import('../views/app/obat/create-supply.vue')
      },
      {
        path: 'obats/:id_obat/supplies/:id_supply/edit',
        props: true,
        component: () => import('../views/app/obat/edit-supply.vue')
      },
      {
        path: 'obats/:id/supplies',
        props: true,
        component: () => import('../views/app/obat/supplies.vue')
      },
      {
        path: 'obats/:id/edit',
        props: true,
        component: () => import('../views/app/obat/edit.vue')
      },
      {
        path: 'settings',
        component: () => import('../views/app/settings.vue')
      },
      {
        path: 'users',
        component: () => import('../views/app/user/list.vue')
      },
      {
        path: 'users/create',
        component: () => import('../views/app/user/create.vue')
      }
    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
