import { Supply, Stok, SistemPembayaran } from '@/types';

function between(x: number, y: number, z: number) {
  return x < y && y < z;
}

function betweenUpIn(x: number, y: number, z: number) {
  return x < y && y <= z;
}

function betweenDownIn(x: number, y: number, z: number) {
  return x <= y && y < z;
}

function betweenIn(x: number, y: number, z: number) {
  return x <= y && y <= z;
}

function mapHarga (harga: number) {
  if (betweenIn(2000, harga, 10000)) return 5;
  if (betweenUpIn(10000, harga, 30000)) return 4;
  if (betweenUpIn(30000, harga, 50000)) return 3;
  if (betweenUpIn(50000, harga, 100000)) return 2;
  if (harga > 100000) return 2;
  throw new Error(`harga value: ${harga}`);
}

function mapDiskon(diskon: number) {
  if (betweenIn(0, diskon, 5)) return 1;
  if (betweenUpIn(5, diskon, 10)) return 3;
  if (betweenUpIn(10, diskon, 14)) return 5;
  throw new Error(`diskon value: ${diskon}`);
}

function mapExpire(x: number) {
  if (betweenIn(1, x, 2)) return 1;
  if (betweenUpIn(2, x, 3)) return 2;
  if (betweenUpIn(3, x, 4)) return 3;
  if (betweenUpIn(4, x, 5)) return 4;
  if (x > 5) return 5;
  throw new Error(`expire value: ${x}`);
}

function mapStok(x: Stok) {
  switch (x) {
    case 'Full': return 5;
    case 'Half': return 3;
    case 'None': return 2;
    default:
      throw new Error(`stok value: ${x}`)
  }
}

function mapJarak(x: number) {
  if (betweenIn(0, x, 3)) return 3;
  if (betweenUpIn(3, x, 5)) return 2;
  if (betweenUpIn(5, x, 7)) return 1;
  throw new Error(`jarak value: ${x}`);
}

function mapSistemPembayaran(x: SistemPembayaran) {
  switch (x) {
    case 'Cepat': return 2;
    case 'Sedang': return 3;
    case 'Lama': return 5;
    default:
      throw new Error(`sistem pembayaran value: ${x}`);
  }
}

export interface Conv {
  harga: number,
  diskon: number,
  expire: number,
  stok: number,
  jarak: number,
  sistemPembayaran: number
}

export function convert (supply: Supply) : Conv {
  return {
    harga: mapHarga(supply.harga),
    diskon: mapDiskon(supply.diskon),
    expire: mapExpire(supply.expire),
    stok: mapStok(supply.stok),
    jarak: mapJarak(supply.jarak),
    sistemPembayaran: mapSistemPembayaran(supply.sistemPembayaran)
  }
}
