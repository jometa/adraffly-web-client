function rupiah(x: number) {
  return 'Rp, ' + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export function format(supplier: any) {
  return {
    harga: rupiah(supplier.harga)
  }
}