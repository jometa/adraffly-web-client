import { range, sum, zip, max } from 'lodash'
import { Conv } from './convert';
import Combinatorials from 'js-combinatorics'

type Pair = [number, number];
type Row = [number, number, number, number, number, number];

type Matrix = Row[];

enum NormError {
    Empty
}

function normalize(matrix: Matrix) : Matrix {
    if (matrix.length == 0) {
        throw NormError.Empty;
    }
    
    let row = matrix[0];
    const m = matrix.length;
    const n = row.length;
    
    // Total for power 2 of each column
    let totalCol = range(n).map(i => 0);
    matrix.forEach(row => {
        row.forEach((val, j) => {
            totalCol[j] += Math.pow(val, 2);
        });
    });
    
    totalCol = totalCol.map(Math.sqrt);
    
    let normed: Matrix = matrix.map(row =>
        row.map((x, j) => x / totalCol[j]) as Row
    );
    
    return normed;
}
    
export function elektre (data: Conv[], weights: number[]) : any {
    const m = data.length;
    const n = weights.length;
    
    let matrix = data.map(item => {
        return [
            item.harga,
            item.diskon,
            item.expire,
            item.stok,
            item.jarak,
            item.sistemPembayaran
        ] as Row;
    });
    
    let normed = normalize(matrix);
    
    let V = normed.map(row => row.map((x, j) => x * weights[j]))

    const pairs: Pair[] = Combinatorials.permutation(range(m), 2).toArray()
    
    let concordances = new Map<Pair, Set<number>>()
    let discordances = new Map<Pair, Set<number>>()
    let concordancesInd = new Map<Pair, number>();
    let discordancesInd = new Map<Pair, number>();
    
    pairs.forEach(pair => {
        let [a, b] = pair
        let concordanceAB = new Set<number>();
        let discordanceAB = new Set<number>();
        zip(normed[a], normed[b])
        .forEach(([ xa, xb ], j) => {
            if (xa >= xb) {
                concordanceAB.add(j);
            } else {
                discordanceAB.add(j);
            }
        })
        concordances.set(pair, concordanceAB)
        discordances.set(pair, discordanceAB)
    })
    
    concordances.forEach((items, pair) => {
        let indexVal = sum(weights.filter((val, index) => items.has(index)))
        concordancesInd.set(pair, indexVal)
    })
        
    discordances.forEach((items, pair) => {
        let nom = 0;
        let [a, b] = pair
        if (items.size != 0) {
            nom = max(Array.from(items.values()).map(j => Math.abs(normed[a][j] - normed[b][j])));
        }
        let denom = max(zip(normed[a], normed[b]).map(([ xa, xb ]) => Math.abs(xa - xb)))
                    
        discordancesInd.set(pair, nom / denom);
    })
                
    let concordanceThreshold = sum( Array.from(concordancesInd.values()).filter(it => !isNaN(it)) ) / concordancesInd.size;
    console.log(concordanceThreshold);
    let concordancesDominant = new Map<Pair, number>();
    let discordanceThreshold = sum( Array.from(discordancesInd.values()).filter(it => !isNaN(it)) ) / discordancesInd.size;
    console.log('discordance: ', discordanceThreshold);
    let discordancesDominant = new Map<Pair, number>();
                    
    Array.from(concordancesInd.entries()).forEach(entry => {
        let [ pair, indexVal ] = entry
        let val = 0
        if (indexVal > concordanceThreshold) {
            val = 1
        }
        concordancesDominant.set(pair, val)
    })
                    
    Array.from(discordancesInd.entries()).forEach(entry => {
        let [ pair, indexVal ] = entry
        let val = 0
        if (indexVal > discordanceThreshold) {
            val = 1
        }
        discordancesDominant.set(pair, val)
    })

    let aggregate = range(m).map(i => range(m).map(i => 0));
    pairs.forEach(pair => {
        let [a, b] = pair;
        if (a == b) {
            aggregate[a][b] = null;
            return;
        }
        if (concordancesDominant.get(pair) == 1 && discordancesDominant.get(pair) == 1) {
            aggregate[a][b] = 1;
        }
    })
    console.log(aggregate)

    return {
        aggregate
    };
}