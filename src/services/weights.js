export const defaults = {
  harga:5,
  diskon:5,
  expire:4,
  stok:5,
  jarak:4,
  sistemPembayaran:3
}

export function get_weights () {
  const raw = localStorage.getItem('adraffy.weights')
  if (!raw) {
    return defaults
  }
  const data = JSON.parse(raw)
  return data
}

export function set_weights (data) {
  localStorage.setItem('adraffy.weights', JSON.stringify(data))
}
