import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import './registerServiceWorker'
import './styles/index.css'
import router from './router'
import { createProvider } from './vue-apollo'
import EmptyData from './components/EmptyData.vue'

Vue.component('empty-data', EmptyData)
Vue.use(Vuelidate)
Vue.config.productionTip = false

new Vue({
  router,
  apolloProvider: createProvider(),
  render: h => h(App)
} as any).$mount('#app')
