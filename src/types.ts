export type Stok = 'Full' | 'Half' | 'None';
export type SistemPembayaran = 'Cepat' | 'Sedang' | 'Lama';

export interface Supplier {
  id?: string;
  harga: number;
  nama: string;
  diskon: number;
  expire: number;
  stok: Stok;
  jarak: number;
  sistemPembayaran: SistemPembayaran;
}

export interface Supply {
  id?: string;
  harga: number;
  diskon: number;
  expire: number;
  stok: Stok;
  jarak: number;
  sistemPembayaran: SistemPembayaran;
}

export interface Obat {
  id?: number;
  nama: string;
}