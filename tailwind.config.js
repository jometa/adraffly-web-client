module.exports = {
  theme: {
    extend: {
      fontFamily: {
        fredoka: ['fredoka', 'serif'],
        baloo2: ['baloo2', 'serif']
      }
    },
  },
  variants: {
    opacity: ['disabled']
  },
  plugins: [],
}
